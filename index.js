// Answer 1. require
// Answer 2. HTTP
// Answer 3. HTTP.createserver
// Answer 4. response.writeHead
// Answer 5. Terminal
// Answer 6. request.url
console.log("welcome server is running")

const HTTP = require(`http`);

HTTP.createServer((request,response)=>{

    if(request.url == `/login`){
        response.writeHead(200,{"Content-Type":"text/plain"});
        response.write("welcome to the login page");
        response.end()
    } else {
        response.writeHead(400,{"Content-Type":"text/plain"});
        response.write("I'm sorry the page you are looking for cannot be found")
        response.end();
    }
}).listen(3000)

